#version 330 core

// Input vertex data
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal_modelspace;
layout(location = 2) in vec2 vertexUV;

out float gl_ClipDistance[1];

out vec3 position_worldspace;
out vec3 normal_cameraspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;
out vec2 UV;


uniform mat4 MVP; // ModelViewProjection
uniform mat4 V; // View
uniform mat4 M; // Model
uniform mat4 MV; // ModelView
uniform mat4 NM; // Normal Matrix, the transpose inverse of the ModelView matrix
uniform vec3 lightPosition_worldspace;
uniform vec4 userClipPlane0; // user defined clip plane in view-space

void main()
{
	gl_Position =  MVP * vec4(vertexPosition_modelspace,1);

	// Calculate data needed for lighing in fragment shader	
	vec3 vertexPosition_cameraspace = ( MV * vec4(vertexPosition_modelspace,1)).xyz;

	// Position of the vertex in worldspace
	position_worldspace = (M * vec4(vertexPosition_modelspace,1)).xyz;

	// Need homogenus space location for the clipping
	vec4 position_worldspaceVec4 = vec4(position_worldspace.x, position_worldspace.y, position_worldspace.z, 1);

	gl_ClipDistance[0] = dot(position_worldspaceVec4, userClipPlane0);

	vec3 cameraPosition = vec3(0,0,0); // OpenGLs camera position is always fixed at 0,0,0 looking down the negativite Z axis.
	eyeDirection_cameraspace = cameraPosition - vertexPosition_cameraspace;

	vec3 lightPosition_cameraspace = ( V * vec4(lightPosition_worldspace,1)).xyz;
	lightDirection_cameraspace = lightPosition_cameraspace - vertexPosition_cameraspace;
	
	normal_cameraspace = mat3(NM) * vertexNormal_modelspace;

	UV = vertexUV;
}

