#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include "player.h"
#include "handleInput.h"


using namespace std;


InputState::InputState()
{
	m_pressedFireBallFirstTime = false;
	m_pressedFireBallLastTime = 0;

	m_playerTwoPressedFireBallFirstTime = false;
    m_playerTwoPressedFireBallLastTime = 0;

	m_enterKeyReleased = true;
}

InputState::~InputState()
{

}

double InputState::PressedFireBallLastTime() const
{
	return m_pressedFireBallLastTime;
}

void InputState::PressedFireBallLastTime(double lastTime)
{
	m_pressedFireBallLastTime = lastTime;
}

bool InputState::PressedFireBallFirstTime() const
{
	return m_pressedFireBallFirstTime;
}

void InputState::PressedFireBallFirstTime(bool PressedFireBallFirstTime)
{
	m_pressedFireBallFirstTime = PressedFireBallFirstTime;
}

////////////
double InputState::PlayerTwoPressedFireBallLastTime() const
{
	return m_playerTwoPressedFireBallLastTime;
}

void InputState::PlayerTwoPressedFireBallLastTime(double lastTime)
{
	m_playerTwoPressedFireBallLastTime = lastTime;
}

bool InputState::PlayerTwoPressedFireBallFirstTime() const
{
	return m_playerTwoPressedFireBallFirstTime;
}

void InputState::PlayerTwoPressedFireBallFirstTime(bool PressedFireBallFirstTime)
{
	m_playerTwoPressedFireBallFirstTime = PressedFireBallFirstTime;
}

bool InputState::EnterKeyReleased() const
{
	return m_enterKeyReleased;
}
void InputState::EnterKeyReleased(bool enterKeyReleased)
{
	m_enterKeyReleased = enterKeyReleased;
}


/////////////

void HandleKeyInput(GLFWwindow* window, InputState &inputState, double deltaTime, Player* playerOne, Player* playerTwo, 
					bool &restartGame, bool gameOver)
{
	double currentTime = glfwGetTime();

	const float rotSpeed = 230.0f;
	const float moveSpeed = 1.5f;

	restartGame = false;

	/* Player one scheme */
	if(playerOne->GetHealth() > 0 && !gameOver)
	{
		if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
		{
			playerOne->SetMoveSpeed(moveSpeed);
		}	

		if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
		{
			playerOne->SetFacingRotationImmediate(playerOne->GetFacingRotation() - deltaTime * rotSpeed);
		}
		
		if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
		{
			playerOne->SetFacingRotationImmediate(playerOne->GetFacingRotation() + deltaTime * rotSpeed);
		}

		int keyToFirePlayerOne = playerTwo ? GLFW_KEY_RIGHT_CONTROL : GLFW_KEY_SPACE;
		if (glfwGetKey(window, keyToFirePlayerOne ) == GLFW_PRESS)
		{
			bool pressedFireballFirstTime = false;				
			if(!inputState.PressedFireBallFirstTime())
			{
				inputState.PressedFireBallFirstTime(true);
				inputState.PressedFireBallLastTime(currentTime);
				pressedFireballFirstTime = true;
			}

			float timeDiffFireBallShot = currentTime - inputState.PressedFireBallLastTime();
			if(timeDiffFireBallShot > 0.2f || pressedFireballFirstTime)
			{
				playerOne->FireBall();
				inputState.PressedFireBallLastTime(currentTime);
			}

		}
	}
	
	if(gameOver)
	{
		if (glfwGetKey(window, GLFW_KEY_ENTER ) == GLFW_PRESS && inputState.EnterKeyReleased())
		{
			restartGame = true;
			inputState.EnterKeyReleased(false);
		}
	}

	if (glfwGetKey(window, GLFW_KEY_ENTER ) != GLFW_PRESS)
	{
		inputState.EnterKeyReleased(true);
	}

	/*********************************************************/

	/* Player two scheme */	
	if(playerTwo && playerTwo->GetHealth() > 0)
	{
		if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS)
		{
			playerTwo->SetMoveSpeed(moveSpeed);
		}	

		if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS)
		{
			playerTwo->SetFacingRotationImmediate(playerTwo->GetFacingRotation() - deltaTime * rotSpeed);
		}
		
		if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS)
		{
			playerTwo->SetFacingRotationImmediate(playerTwo->GetFacingRotation() + deltaTime * rotSpeed);
		}

		if (glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS)
		{
			bool pressedFireballFirstTime = false;				
			if(!inputState.PlayerTwoPressedFireBallFirstTime())
			{
				inputState.PlayerTwoPressedFireBallFirstTime(true);
				inputState.PlayerTwoPressedFireBallLastTime(currentTime);
				pressedFireballFirstTime = true;
			}

			float timeDiffFireBallShot = currentTime - inputState.PlayerTwoPressedFireBallLastTime();
			if(timeDiffFireBallShot > 0.2f || pressedFireballFirstTime)
			{
				playerTwo->FireBall();
				inputState.PlayerTwoPressedFireBallLastTime(currentTime);
			}
		}
	}
}