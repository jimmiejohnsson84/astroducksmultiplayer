// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "initGL.h"
#include "model3d.h"
#include "duck.h"
#include "quadTree.h"

const float g_xMin = -5.5f;
const float g_xMax =  5.5f;
const float g_yMin = -4.0f;
const float g_yMax =  4.0f;

#define DUCKTESTSIZE 0.5f


void InitDuck(Duck* duck, glm::vec3 pos, float scale, int duckSize)
{
	duck->SetPos(pos);
	duck->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
	duck->SetScale(scale);
	duck->SetDuckSize(duckSize);
	duck->SetFacingRotationImmediate(270.0f);	
	duck->SetMoveSpeed(0.0f);
}

bool testNoSplitBeforeLimitAndDepth()
{
	bool status = true;
	// Alter default settings for our test cases
	QuadtreeDucks::s_quadTreeMaxOjbs = 2;
	QuadtreeDucks::s_quadTreeMaxLevels = 2;

	std::vector<Duck* > ducks;

	//--- First test case, make sure no split has occured if inserting < QuadtreeDucks::s_quadTreeMaxOjbs ducks ---
	Duck* duck1 = new Duck();
	Duck* duck2 = new Duck();

	InitDuck(duck1, glm::vec3(0.0f, 0.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck1);

	InitDuck(duck2, glm::vec3(3.0f, 0.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck2);	

	// Create data for the quad-tree
	Duck::CollisionTick(ducks, 0.0f);
	
	QuadtreeDucks testCase0(0);
	testCase0.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

	// Generate TryMove position using CollisionTick
	Duck::CollisionTick(ducks, 0.0f);

	// Insert into the tree
	for(auto &duck : ducks)
	{
		testCase0.Insert(duck);
	}

	// Evaluate
	if(!testCase0.VerifyNrDucks(2))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyNrDucks incorrect!" << endl;
	}
	if(!testCase0.VerifyTreeDepth(0))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyTreeDepth incorrect!" << endl;		
	}

	// Cleanup
	for(auto duck: ducks)
	{
		delete duck;
	}
	ducks.clear();
	// --- End of test case
		
	//--- Second test case, make sure split does happen if inserting > QuadtreeDucks::s_quadTreeMaxOjbs ducks ---
	duck1 = new Duck();
	duck2 = new Duck();
	Duck* duck3 = new Duck();

	InitDuck(duck1, glm::vec3(-1.0f, 0.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck1);

	InitDuck(duck2, glm::vec3(0.0f, 0.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck2);	

	InitDuck(duck3, glm::vec3(1.0f, 0.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck3);

	// Generate TryMove position using CollisionTick
	Duck::CollisionTick(ducks, 0.0f);

	QuadtreeDucks testCase1(0);
	testCase1.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

	// Insert into the tree
	for(auto &duck : ducks)
	{
		testCase1.Insert(duck);
	}

	// Evaluate
	if(!testCase1.VerifyNrDucks(3))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyNrDucks incorrect!" << endl;
	}
	if(!testCase1.VerifyTreeDepth(1))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyTreeDepth incorrect!" << endl;		
	}

	// Cleanup
	for(auto duck: ducks)
	{
		delete duck;
	}
	ducks.clear();
	// --- End of test case

	//--- Third test case, make sure we dont allow a tree to get deeper than s_quadTreeMaxLevels
	duck1 = new Duck();
	duck2 = new Duck();
	duck3 = new Duck();
	Duck* duck4 = new Duck();

	InitDuck(duck1, glm::vec3(-5.0f, -4.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck1);

	InitDuck(duck2, glm::vec3(-4.5f, -4.0f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck2);	

	InitDuck(duck3, glm::vec3(-4.0f, -3.5f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck3);	

	InitDuck(duck4, glm::vec3(-3.5f, -3.5f, -10.0f), DUCKBIG, 3);
	ducks.push_back(duck4);	

	// Generate TryMove position using CollisionTick
	Duck::CollisionTick(ducks, 0.0f);	

	QuadtreeDucks testCase2(0);
	testCase2.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

	// Insert into the tree
	for(auto &duck : ducks)
	{
		testCase2.Insert(duck);
	}

	// Evaluate
	if(!testCase2.VerifyNrDucks(4))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyNrDucks incorrect!" << endl;
	}
	if(!testCase2.VerifyTreeDepth(2))
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testNoSplitBeforeLimitAndDepth::VerifyTreeDepth incorrect!" << endl;		
	}

	// Cleanup
	for(auto duck: ducks)
	{
		delete duck;
	}
	ducks.clear();
	// --- End of test case	

	return status;
}


bool testCollidablesSplittingLines(std::vector<Duck*> &collidablesResult, std::vector<Duck* > ducks)
{
	bool status = true;
	if(collidablesResult.size() != 5)
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testInsertAndGetCollidables:: Incorrect number of collidables!" << endl;				
	}
	else
	{
		if(collidablesResult[0] != ducks[1] || collidablesResult[1] != ducks[3] || collidablesResult[2] != ducks[4] ||
			collidablesResult[3] != ducks[5] || collidablesResult[4] != ducks[7])
		{
			status = false;
			cout << "    Test failed!" << endl;
			cout << "    testInsertAndGetCollidables:: incorrect collidable duck!" << endl;							
		}
	}
	return status;
}

bool testCollidablesQuadrant(std::vector<Duck*> &collidablesResult, Duck* expectedDuck)
{
	bool status = true;
	if(collidablesResult.size() != 1)
	{
		status = false;
		cout << "    Test failed!" << endl;
		cout << "    testInsertAndGetCollidables:: Incorrect number of collidables!" << endl;				
	}
	else
	{
		if(collidablesResult[0] != expectedDuck)
		{
			status = false;
			cout << "    Test failed!" << endl;
			cout << "    testInsertAndGetCollidables:: incorrect collidable duck!" << endl;							
		}
	}
	return status;

}

std::vector<Duck* > createDucksForGetCollidablesTest()
{
	std::vector<Duck* > ducks;

	for(int i = 0; i < 18; i++)
	{
		Duck* duck = new Duck();
		ducks.push_back(duck);
	}

	return ducks;
}

void InitDucksForCollidablesTest(int index, std::vector<Duck* > &ducks, const float yPos)
{
	InitDuck(ducks[index], 		glm::vec3(-4.0f, yPos, -10.0f), DUCKTESTSIZE, 3);
	InitDuck(ducks[index + 1], 	glm::vec3(-1.0f, yPos, -10.0f), DUCKTESTSIZE, 3);
	InitDuck(ducks[index + 2], 	glm::vec3(1.0f,  yPos, -10.0f), DUCKTESTSIZE, 3);
	InitDuck(ducks[index + 3], 	glm::vec3(4.0f,  yPos, -10.0f), DUCKTESTSIZE, 3);
}

void evaluteGetDuckPosFromIndex(int i, int j, float &duckX, float &duckY)
{
	switch(j)
	{
		case 0:
			duckX = -4.0f;
		break;

		case 1:
			duckX = -1.0f;
		break;

		case 2:
			duckX = 1.0f;
		break;

		case 3:
			duckX = 4.0f;
		break;				
	}

	switch(i)
	{
		case 0:
			duckY = -3.0f;
		break;

		case 1:
			duckY = -1.0f;
		break;

		case 2:
			duckY = 1.0f;
		break;

		case 3:
			duckY = 3.0f;
		break;
		
	}	
}

/*
	Test Insert and GetCollidables interface.

	We want a quad tree with 2 levels in it, so this picture bellow represent the world split twice.
	We also want:

	* A duck in each quadrant
	* A duck that splits one splitting line in the first quadrant (level 1 in quadtree)
	* A duck that splits two splittings lines in the first quadrant (level 1 in quadtree)

	Note orientation of quadrants! Q1 is at the top here because we consider the top left position to be
	the min X and min Y, which is (-5.5, -4.0) in our case. This is different than if we would have let the bottom
	left position be min X and min Y. In this layout X+ is to the right but Y+ is inverted from "the normal"

	--> X +
	|
	|
	Y
	+
	
     Q11   Q12    Q21    Q22
	|-----------------------|
	|     |     |      |    |
	|  X  |  X (X)  X  |  X |
	|-----------------------|
Q14	|     |     |      |    | Q23
	|  X  |  X  |  X   |  X |
	-----------(X)-----------      
Q41	|     |     |      |    | Q32
	|  X  |  X  |  X   |  X | 
	|-----------------------|
	|     |     |      |    |
	|  X  |  X  |  X   |  X |
	|-----------------------|
      Q44   Q43   Q34    Q33

	X represent ducks
	16 + 2 ducks (one in each quad + ducks splitting line(s))
*/	

bool testInsertAndGetCollidables()
{
	bool status = true;
	// Alter default settings for our test cases
	QuadtreeDucks::s_quadTreeMaxOjbs = 2;
	QuadtreeDucks::s_quadTreeMaxLevels = 2;

	std::vector<Duck* > ducks = createDucksForGetCollidablesTest();

	const int firstDuckDebugId = ducks[0]->GetDebugId();
	InitDucksForCollidablesTest(0, ducks, -3.0f);
	InitDuck(ducks[4], glm::vec3(0.0f, -3.0f, -10.0f), DUCKTESTSIZE, 3); // SPLITTING ducks, Spans Q12 and Q21
	
	InitDucksForCollidablesTest(5, ducks, -1.0f);
	InitDuck(ducks[9], glm::vec3(0.0f, 0.0f, -10.0f), DUCKTESTSIZE, 3); // SPLITTING DUCK, Spans all quadrants (origo position)
	
	InitDucksForCollidablesTest(10, ducks, 1.0f);
	InitDucksForCollidablesTest(14, ducks, 3.0f);

	// Generate TryMove position using CollisionTick
	Duck::CollisionTick(ducks, 0.0f);

	// Evaluate
	Duck* testInsertDuck = new Duck();
	std::vector<Duck*> collisionTickTestDuck;
	collisionTickTestDuck.push_back(testInsertDuck);
	/*
		insert a duck into each quadrant, check that it gets the duck in the quadrant as a collidable + the splitting ducks
	*/
	int expectedFirstCollidable = firstDuckDebugId;
	const int firstSplittingQuadrantDuckIndex = firstDuckDebugId + 4;
	const int secondSplittingQuadrantDuckIndex = firstDuckDebugId + 9;
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			float duckX, duckY;
			evaluteGetDuckPosFromIndex(i, j, duckX, duckY);

			InitDuck(testInsertDuck, glm::vec3(duckX, duckY, -10.0f), DUCKTESTSIZE, 3);

			// Got to generate TryMove position for this duck as well
			Duck::CollisionTick(collisionTickTestDuck, 0.0f);

			QuadtreeDucks testCase(0);
			testCase.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

			// Insert ducks into the tree
			for(auto &duck : ducks)
			{
				testCase.Insert(duck);
			}

			std::vector<Duck*> collidables;
			testCase.GetCollidables(testInsertDuck, collidables);

			if(collidables.size() != 3)
			{
				cout << "    Test failed!" << endl;
				cout << "    collidables size incorrect" << endl;
				status = false;
			}
			else
			{
				if(collidables[0]->GetDebugId() != expectedFirstCollidable)
				{
					cout << "    Test failed!" << endl;
					cout << "    got incorrect collidable result" << endl;
					status = false;
				}

				if(collidables[1]->GetDebugId() != firstSplittingQuadrantDuckIndex)
				{
					cout << "    Test failed!" << endl;
					cout << "    got incorrect collidable result" << endl;
					status = false;
				}

				if(collidables[2]->GetDebugId() != secondSplittingQuadrantDuckIndex)
				{
					cout << "    Test failed!" << endl;
					cout << "    got incorrect collidable result" << endl;
					status = false;
				}
			}
			expectedFirstCollidable++;

			// Splitting ducks index, skip ahead to next
			if(	expectedFirstCollidable == firstSplittingQuadrantDuckIndex ||
				expectedFirstCollidable == secondSplittingQuadrantDuckIndex)
			{
				expectedFirstCollidable++;
			}
		}
	}

	// Also evaluate inserting a duck that splits a quadrant - should get all ducks as collidables
	InitDuck(testInsertDuck, glm::vec3(0.0f, 0.0f, -10.0f), DUCKTESTSIZE, 3);
	// Got to generate TryMove position for this duck as well
	Duck::CollisionTick(collisionTickTestDuck, 0.0f);

	QuadtreeDucks testCase(0);
	testCase.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

	// Insert ducks into the tree
	for(auto &duck : ducks)
	{
		testCase.Insert(duck);
	}

	std::vector<Duck*> collidables;
	testCase.GetCollidables(testInsertDuck, collidables);
	
	if(collidables.size() != 18)
	{
		cout << "    Test failed!" << endl;
		cout << "    collidables size incorrect" << endl;
		status = false;
	}
	else
	{
		std::set<int> collidableIdSet;
		for(auto &duck : collidables)
		{
			if(collidableIdSet.find(duck->GetDebugId()) != collidableIdSet.end())
			{
				cout << "    Test failed!" << endl;
				cout << "    got incorrect collidable result" << endl;
				status = false;
			}
			collidableIdSet.insert(duck->GetDebugId());
		}
	}
	
	// Cleanup
	for(auto duck: ducks)
	{
		delete duck;
	}
	delete testInsertDuck;

	return status;
}


/*
	Second quadrant test, verify that ducks that split a crossing line in a sub-quadrant will be

	Note orientation of quadrants! Q1 is at the top here because we consider the top left position to be
	the min X and min Y, which is (-5.5, -4.0) in our case. This is different than if we would have let the bottom
	left position be min X and min Y. In this layout X+ is to the right but Y+ is inverted from "the normal"

	--> X +
	|
	|
	Y
	+

     Q11   Q12    Q21    Q22
	|-----------------------|
	|    (X)    |      |    |
	|  X  |  X  |  X   |  X |
	|----(X)----------------|
	|     |     |      |    |
	|  X  |  X  |  X   |  X |
	-------------------------
	|     |     |      |    |
	|  X  |  X  |  X   |  X |
	|-----------------------|
	|     |     |      |    |
	|  X  |  X  |  X   |  X |
	|-----------------------|
      Q44   Q43   Q34    Q33
	

	X represent ducks
	16 + 2 ducks (one in each quad + ducks splitting line(s))

*/
bool testInsertAndGetCollidablesCrossingSubQuadrants()
{
	bool status = true;
	// Alter default settings for our test cases
	QuadtreeDucks::s_quadTreeMaxOjbs = 2;
	QuadtreeDucks::s_quadTreeMaxLevels = 2;

	std::vector<Duck* > ducks = createDucksForGetCollidablesTest();

	const int firstDuckDebugId = ducks[0]->GetDebugId();
	InitDucksForCollidablesTest(0, ducks, -3.0f);
	InitDuck(ducks[4], glm::vec3(-2.75f, -3.0f, -10.0f), DUCKTESTSIZE, 3); // SPLITTING duck, Spans Q11 and Q22
	InitDuck(ducks[5], glm::vec3(-2.75f, -2.0f, -10.0f), DUCKTESTSIZE, 3); // SPLITTING duck, Spans Q11, Q12, Q13, Q14
	
	InitDucksForCollidablesTest(6, ducks, -1.0f);	
	InitDucksForCollidablesTest(10, ducks, 1.0f);
	InitDucksForCollidablesTest(14, ducks, 3.0f);

	// Generate TryMove position using CollisionTick
	Duck::CollisionTick(ducks, 0.0f);

	// Evaluate
	Duck* testInsertDuck = new Duck();
	std::vector<Duck*> collisionTickTestDuck;
	collisionTickTestDuck.push_back(testInsertDuck);

	/*
		insert a duck into each quadrant, check that it gets the duck in the quadrant as a collidable + the splitting ducks
	*/
	int expectedFirstCollidable = firstDuckDebugId;
	const int firstSplittingQuadrantDuckIndex = firstDuckDebugId + 4;
	const int secondSplittingQuadrantDuckIndex = firstDuckDebugId + 5;

	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			float duckX, duckY;
			evaluteGetDuckPosFromIndex(i, j, duckX, duckY);

			InitDuck(testInsertDuck, glm::vec3(duckX, duckY, -10.0f), DUCKTESTSIZE, 3);

			// Got to generate TryMove position for this duck as well
			Duck::CollisionTick(collisionTickTestDuck, 0.0f);

			QuadtreeDucks testCase(0);
			testCase.SetWorldSize(g_xMin, g_yMin, g_xMax, g_yMax);

			// Insert ducks into the tree
			for(auto &duck : ducks)
			{
				testCase.Insert(duck);
			}

			std::vector<Duck*> collidables;
			testCase.GetCollidables(testInsertDuck, collidables);

			if( i < 2 && j < 2) // Q11, Q12, Q13, Q14				
			{
				if(collidables.size() != 3)
				{
					cout << "    Test failed!" << endl;
					cout << "    collidables size incorrect" << endl;
					status = false;					
				}
				else
				{
					if(collidables[0]->GetDebugId() != expectedFirstCollidable)
					{
						cout << "    Test failed!" << endl;
						cout << "    got incorrect collidable result" << endl;
						status = false;
					}
					
					if(collidables[1]->GetDebugId() != firstSplittingQuadrantDuckIndex)
					{
						cout << "    Test failed!" << endl;
						cout << "    got incorrect collidable result" << endl;
						status = false;
					}

					if(collidables[2]->GetDebugId() != secondSplittingQuadrantDuckIndex)
					{
						cout << "    Test failed!" << endl;
						cout << "    got incorrect collidable result" << endl;
						status = false;
					}					
				}
			}
			else // All other quadrants
			{
				if(collidables.size() != 1)
				{
					cout << "    Test failed!" << endl;
					cout << "    collidables size incorrect" << endl;
					status = false;
				}
				else
				{
					if(collidables[0]->GetDebugId() != expectedFirstCollidable)
					{
						cout << collidables[0]->GetDebugId() << endl;
						cout << "    Test failed!" << endl;
						cout << "    got incorrect collidable result" << endl;
						status = false;
					}
					
				}
			}

			expectedFirstCollidable++;

			// Splitting ducks index, skip ahead to next
			if(	expectedFirstCollidable == firstSplittingQuadrantDuckIndex)
			{
				expectedFirstCollidable += 2;
			}
		}
	}


	// Cleanup
	for(auto duck: ducks)
	{
		delete duck;
	}
	delete testInsertDuck;
	
	return status;
}

int main( void )
{
	srand(time(NULL)); // Make sure we get new random values from rand

	GLFWwindow* window;

	if(InitGL(&window) == -1)
	{
		return -1;
	}

	Model3d::InitModel3d();
	Duck::InitDucks();
	cout << "--- Test QuadTree Starting ---" << endl;
	cout << "Running testNoSplitBeforeLimitAndDepth" << endl;
	bool status =  testNoSplitBeforeLimitAndDepth();
	cout << "Status testNoSplitBeforeLimitAndDepth: " << status << endl;

	cout << "Running testInsertAndGetCollidables" << endl;
	status = testInsertAndGetCollidables();
	cout << "Status testInsertAndGetCollidables: " << status << endl;

	cout << "Running testInsertAndGetCollidablesCrossingSubQuadrants" << endl;
	status = testInsertAndGetCollidablesCrossingSubQuadrants();
	cout << "Status testInsertAndGetCollidablesCrossingSubQuadrants: " << status << endl;	
	cout << "--- Test QuadTree finished ---" << endl;

	// Clean up and close OpenGL
	glfwTerminate();

	return 0;
}

