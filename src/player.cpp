#include <iostream>
#include <fstream>
using namespace std;
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "model3d.h"
#include "model3d_cooktorrance.h"
#include "player.h"
#include "duck.h"
#include "projectileBall.h"
#include "collisionDetection.h"
#include "lightData.h"
#include "renderStruct.h"
#include "texture.h"
#include "gameDefs.h"

//#define DEBUG

#if defined(_WIN32)
	#define M_PI 3.14159f
#endif


Model3d* Player::m_model = NULL;
Model3dCookTorrance* Player::m_modelCookTorrance = NULL;

//----------------------------------------------------------------------
void Player::InitPlayer()
{
	LoadModel();
}
//----------------------------------------------------------------------
void Player::CollisionTick(std::vector<std::unique_ptr<Player> > &players, float deltaTime)
{
	for(auto &player : players)
	{
		player->TryMove(deltaTime); // Calculate next position
	}		
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
void Player::LoadModel()
{
	m_model = new Model3d();
	m_model->Build("boat.obj");
	m_model->SetTexture(GetTextureResource(FISHINGBOAT_ALBEDO_TEXTURE));

	m_modelCookTorrance = new Model3dCookTorrance();
	m_modelCookTorrance->Build("boat.obj");
	m_modelCookTorrance->SetTexture(GetTextureResource(FISHINGBOAT_ALBEDO_TEXTURE), GetTextureResource(FISHINGBOAT_NORMAL_TEXTURE),
									GetTextureResource(FISHINGBOAT_METALLIC_TEXTURE), GetTextureResource(FISHINGBOAT_ROUGHNESS_TEXTURE),
									GetTextureResource(FISHINGBOAT_AO_TEXTURE));

}
//----------------------------------------------------------------------
Player::Player(bool isSecondPlayer, const GameDefData& gameDefData) : m_gameDefData(gameDefData)
{
	m_moveDir = glm::vec3(0.0f, 0.0f, 0.0f);
		
	m_pos = glm::vec3(0.0f, 0.0f, 0.0f);
	m_rot = glm::vec3(0.0f, 0.0f, 0.0f);
	m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
	m_color = glm::vec3(0.7f, 0.0f ,0.0f);
	m_moveSpeed = 0.0f;

	m_wobbleStartTime = -1.0f;
	m_wobbleCycleTime = 1.0f;
	m_wobbleAmount = 40.0f;	

	m_boundingSphereRadius = 2.2f;
	ResetScore();
	ResetHealth();
	
	m_playerInvisibleStartTime = -1.0f; // If player takes damage, make player invinsible for a short time
	m_playerInivisible = false;	

	m_isSecondPlayer = isSecondPlayer;
}
//----------------------------------------------------------------------
void Player::ResetScore()
{
	m_score = 0;
	m_scoreNewLife = 0;
}
//----------------------------------------------------------------------
void Player::ResetHealth()
{
	m_health = 3;
}
//----------------------------------------------------------------------
void Player::ResetHealth(int startHealth)
{
	m_health = 2;
}
//----------------------------------------------------------------------
void Player::DecreaseScore(int decrease)
{
	m_score -= decrease;
	if(m_score < 0)
	{
		m_score = 0;
	}
}
//----------------------------------------------------------------------

Player::~Player()
{
}
//----------------------------------------------------------------------
void Player::SetPos(glm::vec3 pos)
{
	m_pos = pos;
}
//----------------------------------------------------------------------
void Player::SetRot(glm::vec3 rot)
{
	m_rot = rot;
}
//----------------------------------------------------------------------
void Player::SetScale(float scale)
{
	m_scale = glm::vec3(scale * 1.2f, scale, scale);
	m_boundingSphereRadius = 2.2f * scale;
}
//----------------------------------------------------------------------
void Player::SetFacingRotationImmediate(float facingRotation)
{
	m_rot.y = facingRotation;

	// Model faces negative Y per default, so need to offset the rotation (0 degrees = facing -Y)
	float rotationWithOffset = m_rot.y - 90.0f;
	float rotationWithOffsetAsRadians = glm::radians(rotationWithOffset);
	m_moveDir = glm::vec3(cos(rotationWithOffsetAsRadians), sin(rotationWithOffsetAsRadians), 0.0f);	
}
//----------------------------------------------------------------------
void Player::SetMoveDir(glm::vec3 dir)
{
	m_moveDir = glm::normalize(dir);

	// Auto rotate around y axis to face direction we move in
	const glm::vec3 defaultFacingDir(0.0f, -1.0f, 0.0f);
	m_rot.y = acos(glm::dot(defaultFacingDir, m_moveDir)) * 180.0 / M_PI;

	if(dir.x < 0.0f)
	{
		m_rot.y = 360.0f - m_rot.y;
	}
}
//----------------------------------------------------------------------
void Player::SetMoveSpeed(float speed)
{
	m_moveSpeed = speed;
}
//----------------------------------------------------------------------
void Player::SetColor(glm::vec3 color)
{
	m_color = color;
}
//----------------------------------------------------------------------
void Player::TakeDamage(int damage)
{
	if(m_health > 0)
	{
		if(!m_playerInivisible)
		{
			m_health -= damage;

			m_playerInvisibleStartTime = glfwGetTime();
			m_playerInivisible = true;
		}		
	}
}
//----------------------------------------------------------------------
void Player::IncreaseScore(int score)
{
	m_score += score;
	m_scoreNewLife += score;


	if(m_scoreNewLife > 1000)
	{
		m_scoreNewLife = 0;

		if(m_health < 5)
		{
			m_health++;
		}
	}

}
//----------------------------------------------------------------------
void Player::HitWorldBorder()
{
	m_hitWorldBorder = true;
}
//----------------------------------------------------------------------
void Player::FireBall()
{
	// Create a ProjectileBall that flies in the same direction as the boat is facing
	
	std::unique_ptr<ProjectileBall> ball = make_unique<ProjectileBall>();

	const float forwardBallFactor = 0.5f;
	glm::vec3 ballPos = m_pos + (m_moveDir * forwardBallFactor);
	ball->SetPos(ballPos);
	ball->SetScale(0.12f);
	ball->SetMoveDir(m_moveDir);
	ball->SetMoveSpeed(4.5f);

	m_projectileBalls.push_back(std::move(ball));
}
//----------------------------------------------------------------------
int Player::GetHealth() const
{
	return m_health;
}
//----------------------------------------------------------------------
int Player::GetScore() const
{
	return m_score;
}
//----------------------------------------------------------------------
glm::vec3& Player::GetPos()
{
	return m_pos;
}
//----------------------------------------------------------------------
glm::vec3& Player::GetPosTry()
{
	return m_posTry;
}
//----------------------------------------------------------------------
glm::vec3&	Player::GetMoveDir()
{
	return m_moveDir;
}
//----------------------------------------------------------------------
glm::vec3&	Player::GetRot()
{
	return m_rot;
}
//----------------------------------------------------------------------
float& Player::GetBoundingSphereRadius()
{
	return m_boundingSphereRadius;
}
//----------------------------------------------------------------------
std::vector<std::unique_ptr<ProjectileBall> >& Player::GetProjectileBalls()
{
	return m_projectileBalls;
}
//----------------------------------------------------------------------
float& Player::GetMoveSpeed()
{
	return m_moveSpeed;
}
//----------------------------------------------------------------------
float& Player::GetFacingRotation()
{
	return m_rot.y;
}
//----------------------------------------------------------------------
void Player::TryMove(float deltaTime)
{
	m_posTry = m_pos;
	DoMove(deltaTime, m_posTry);
}
//----------------------------------------------------------------------
void Player::DoWobble()
{
	if( m_wobbleStartTime < 0.0f)
	{
		m_wobbleStartTime = float(glfwGetTime());
	}

	float deltaTime = float(glfwGetTime()) - m_wobbleStartTime;

	if(deltaTime > m_wobbleCycleTime)
	{
		m_wobbleStartTime = -1.0f;
	}
	else
	{
		float t = deltaTime / m_wobbleCycleTime;

		if(t > 0.5f)
		{
			m_rot.z = (m_wobbleAmount * 0.5f) - (t - 0.5f) * m_wobbleAmount;
		}
		else
		{
			m_rot.z = t * m_wobbleAmount;
		}
	}
}
//----------------------------------------------------------------------
void Player::TickProjectileBalls(float deltaTime)
{
	int i = 0;
	while(i < m_projectileBalls.size() && !m_projectileBalls.empty())
	{
		ProjectileBall* projectileBall = m_projectileBalls[i].get();
		projectileBall->Tick(deltaTime);
		if(projectileBall->GetDestroyed())
		{
			m_projectileBalls.erase(m_projectileBalls.begin() + i);
		}
		else
		{
			i++;
		}
	}
}
//----------------------------------------------------------------------
void Player::Tick(float deltaTime)
{
	DoWobble();
	
	if(!m_hitWorldBorder)
	{
		DoMove(deltaTime, m_pos);
	}
	m_hitWorldBorder = false;	

	if(m_playerInivisible)
	{
		double timeNow = glfwGetTime();

		if(timeNow - m_playerInvisibleStartTime > 1.0f)
		{
			m_playerInvisibleStartTime = -1.0f;
			m_playerInivisible = false;
		}		
	}
	
	TickProjectileBalls(deltaTime);
}
//----------------------------------------------------------------------
void Player::DestroyProjectile(int projectileCounter)
{
	m_projectileBalls[projectileCounter]->Destroy();
}
//----------------------------------------------------------------------
void Player::DoMove(float deltaTime, glm::vec3 &pos)
{
	float moveSpeedTimeAdjusted = m_moveSpeed * deltaTime;
	pos += m_moveDir * moveSpeedTimeAdjusted;

	// Lose speeed gradually
	const float speedFallofFactor = 0.5f;
	m_moveSpeed = m_moveSpeed - speedFallofFactor * deltaTime;
	if(m_moveSpeed < 0.0f)
	{
		m_moveSpeed = 0.0f;
	}

	DoUpdateWaterLevel(pos);
}
//----------------------------------------------------------------------
void Player::DoUpdateWaterLevel(glm::vec3 &pos)
{
	pos.z = m_gameDefData.GetPlayerDepth();
}
//----------------------------------------------------------------------
void Player::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection,  const glm::mat4 &view)
{
	#ifdef USE_PBR
		if(m_isSecondPlayer)
		{
			m_modelCookTorrance->SetTexture(GetTextureResource(FISHINGBOAT_PLAYER_TWO_ALBEDO_TEXTURE), GetTextureResource(FISHINGBOAT_NORMAL_TEXTURE),
											GetTextureResource(FISHINGBOAT_METALLIC_TEXTURE), GetTextureResource(FISHINGBOAT_ROUGHNESS_TEXTURE),
											GetTextureResource(FISHINGBOAT_AO_TEXTURE));
		}
		else
		{
			m_modelCookTorrance->SetTexture(GetTextureResource(FISHINGBOAT_ALBEDO_TEXTURE), GetTextureResource(FISHINGBOAT_NORMAL_TEXTURE),
											GetTextureResource(FISHINGBOAT_METALLIC_TEXTURE), GetTextureResource(FISHINGBOAT_ROUGHNESS_TEXTURE),
											GetTextureResource(FISHINGBOAT_AO_TEXTURE));
			
		}

		m_modelCookTorrance->Render(renderState, projection, view, m_pos, m_rot, m_scale);
	#else
		if(m_isSecondPlayer)
		{
			m_model->SetTexture(GetTextureResource(FISHINGBOAT_PLAYER_TWO_ALBEDO_TEXTURE));
		}
		else
		{
			m_model->SetTexture(GetTextureResource(FISHINGBOAT_ALBEDO_TEXTURE));
		}

		m_model->Render(renderState, projection, view, m_pos, m_rot, m_scale);
	#endif	
}