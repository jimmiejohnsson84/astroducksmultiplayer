#ifndef TEXT2D_HPP
#define TEXT2D_HPP

#include <vector>

class Text2D
{
    public:
    Text2D(char *text, glm::vec3 start, float size = 37.0f);
    ~Text2D();

    static void		InitText2D();
    
    void            Render(const glm::mat4 &projection, const glm::mat4 &view);
    void            UpdateText(char * text); // Your not allowed to change the length of the text here

    private:

    void            CreateTextQuads();
    void            BuildVBOS();

    char*                       m_text;
    int                         m_textLength;

    float                       m_size;
    glm::vec3                   m_start;

	std::vector<glm::vec3>		m_vertices;
	std::vector<glm::vec2>		m_verticesUV; 

	GLuint	                    m_verticesVBO;
	GLuint	                    m_verticesUVVBO;    

	// Common data
	static GLuint   m_programID;
	static GLuint   m_mvpID;
	static GLuint   m_textureID;

    static GLuint   m_text2DTextureID;
	
};

#endif