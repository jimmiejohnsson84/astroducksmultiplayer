#ifndef PROJECTILEBALL_H
#define PROJECTILEBALL_H

#include <sstream>
#include <vector>
#include <memory>

// Fwds
class Model3d;
class Model3dCookTorrance;
struct RenderStateModel3D;



class ProjectileBall
{
	public:
	ProjectileBall();
	~ProjectileBall();

	void 	SetPos(glm::vec3 pos);
	void 	SetScale(float scale);
	void 	SetMoveDir(glm::vec3 dir);		
	void	SetMoveSpeed(float speed);

	glm::vec3&		GetPos();
	glm::vec3&		GetMoveDir();
	float& 			GetMoveSpeed();
	float&			GetBoundingSphereRadius();
	bool			GetDestroyed() const;
	void			Destroy();
	
	void Tick(float deltaTime);

	void Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, const glm::mat4 &view);	

	static void InitProjectileBall();

	private:

	static void	LoadModel();

	void	DoMove(float deltaTime, glm::vec3 &pos);

	glm::vec3 	m_moveDir;
	glm::vec3 	m_rot;
	glm::vec3 	m_pos;
	glm::vec3	m_scale;
	float		m_moveSpeed;
	float		m_boundingSphereRadius;

	bool		m_destroy;

	static Model3d* m_model;
	static Model3dCookTorrance* m_modelCookTorrance;
};

#endif