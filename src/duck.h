#ifndef DUCK_H
#define DUCK_H

#include <sstream>
#include <vector>
#include <memory>

// Fwds
class Model3d;
class Model3dCookTorrance;
class GameDefData;
struct RenderStateModel3D;

class DuckDestroyResult
{
	public:

	DuckDestroyResult() { m_increaseWaterLevel = 0.0f; m_decreaseWaterLevel = 0.0f;}
	~DuckDestroyResult() {}

	float m_increaseWaterLevel;
	float m_decreaseWaterLevel;
};


struct	CollisionData
{
	glm::vec3 		m_posTry;
	float			m_boundingSphereRadius;
	CollisionData*	m_nextFree;
	unsigned char	m_collsionPoolIndex;
};


float DuckDepth(int duckSize, const GameDefData* gameDefData);

#define MAX_NR_DUCKS 100

class Duck
{
	public:

	class DuckPool
	{
		public:
		static DuckPool& GetInstance();

		~DuckPool();

		Duck*		GetDuck();
		void		ReturnDuckToFreePool(Duck* p);

		private:

		DuckPool();

		Duck* m_firstFree;
		std::vector<Duck> m_ducks;

		static const GameDefData* m_gameDefData;
	};

	Duck();
	~Duck();

	void	Init();
	void 	SetPos(glm::vec3 pos);
	void 	SetRot(glm::vec3 rot);
	void 	SetScale(float scale);
	void 	SetFacingRotation(float facingRotation);
	void 	SetFacingRotationImmediate(float facingRotation);
	void 	SetMoveDir(glm::vec3 dir);		
	void	SetDuckSize(unsigned int duckSize);
	void	SetMoveSpeed(float speed);
	void	SetSpawnWaterDrainingDuck(bool b);

	glm::vec3&		GetPos();
	glm::vec3&		GetPosTry();
	glm::vec3&		GetMoveDir();
	float&			GetBoundingSphereRadius();
	float&			GetMoveSpeed();
	float&			GetMoveSpeedOrg();
	float&			GetFacingRotation();
	unsigned int& 	GetDuckSize();
	int				GetDuckTexturePack() const;
	bool			GetDestroyed();
	unsigned char	GetCollisionDataIndex() const;

	int				GetDebugId() const;


	void RememberSpeed();
	void ResetSpeed();
	void BounceBack();
	void Divert(float divertAngle);	
	void Tick(float deltaTime);
	void Destroy();
	void Collided();


	void Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, const glm::mat4 &view);	

	static void InitDucks(const GameDefData* gameDefData);
	static bool DuckCollidesWithOtherDucks(Duck* duck, std::vector<Duck* >&otherDucks);
	static bool DuckInsideWorldBorder(Duck* duck);
	static void Destroy(Duck* duck, const GameDefData* gameDefData, DuckDestroyResult &result);
	static void CollisionTick(std::vector<Duck* > &ducks, float deltaTime);	
	static void Tick(std::vector<Duck* > &ducks, float deltaTime);
	
	private:

	void	TryMove(float deltaTime);
	
	bool	BounceBackActive() const;
	void	StartBounceBack();
	void	DoBounceBack();
	void	DoRotation();
	void	DoWobble();
	
	void	DoAutoDestroy();
	void	DoMove(float deltaTime, glm::vec3 &pos);
	void 	DoUpdateWaterLevel(glm::vec3 &pos);

	static void LoadModel();
	static void CreateSplitMatrices();
	
	CollisionData *m_collisionData;

	glm::vec3 	m_moveDir;
	glm::vec3 	m_bounceBack;
	glm::vec3 	m_pos;
	
	glm::vec3 	m_rot;
	glm::vec3	m_scale;
	float		m_moveSpeed;
	float		m_moveSpeedOrg;
	

	float		m_startRotationAngle;
	float		m_targetRotationAngleDelta;
	float		m_rotationStarTime;
	float		m_rotationDurationTime;

	float		m_wobbleStartTime;
	float		m_wobbleCycleTime;
	float		m_wobbleAmount;
	
	float		m_bounceBackFactor;
	float		m_bounceBackSpeed;
	float		m_bounceBackStartTime;

	bool		m_autoDestroy;
	float		m_autoDestroyStartTime;
	float		m_autoDestroyLifeTime;

	bool		m_destroy;
	bool		m_collided;

	Duck*		m_nextFree;


	/*
		Size of duck, used to determine if the duck should spawn 3, 2 or 0 ducks if it is destroyed	
	*/
	unsigned int	m_duckSize;

	int				m_duckTexturePack;

	int			m_duckDebugID;

	static Model3dCookTorrance* m_modelCookTorrance;
	static Model3d* m_model;


	static int	m_debugID;

	static glm::mat4 m_rotationSplitDuckRight;
	static glm::mat4 m_rotationSplitDuckLeft;

	static std::vector<Duck*>	m_newDucks;
	static bool					m_hasDestroyDucks;

	static const GameDefData*	m_gameDefData;
};

class CollisionDataPool
{
	public:
	static CollisionDataPool& GetInstance();

	~CollisionDataPool();

	CollisionData*			GetCollisionData();
	const CollisionData&	GetCollsionData(unsigned int index) const;
	void					ReturnCollisionDataToFreePool(CollisionData* p);
	

	private:

	CollisionDataPool();

	CollisionData* m_firstFree;
	std::vector<CollisionData> m_collisionDatas;
};

#endif