// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "objloader.h"
#include "model3d.h"
#include "shader.h"
#include "lightData.h"
#include "renderStruct.h"

Model3d::shaderProgramHandles Model3d::m_model3DShaderHandles;

void Model3d::InitModel3d()
{
	m_model3DShaderHandles.m_programID = LoadShaders( "model3d.vertexshader", "model3d.fragmentshader" );	

	// Get handles for data in the shader
	m_model3DShaderHandles.m_modelViewProjectionID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "MVP");
	m_model3DShaderHandles.m_viewMatrixID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "V");
	m_model3DShaderHandles.m_modelMatrixID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "M");
	m_model3DShaderHandles.m_modelViewMatrixID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "MV");
	m_model3DShaderHandles.m_normalMatrixID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "NM");
	m_model3DShaderHandles.m_lightPos_worldspaceID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "lightPosition_worldspace");
	m_model3DShaderHandles.m_lightColorID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "lightColor");
	m_model3DShaderHandles.m_textureID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "textureSampler");

	m_model3DShaderHandles.m_userClipPlane0ID = glGetUniformLocation(m_model3DShaderHandles.m_programID, "userClipPlane0");
	
}
//----------------------------------------------------------------------
void Model3d::PrepareRender()
{
	glUseProgram(m_model3DShaderHandles.m_programID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
}
//----------------------------------------------------------------------
void Model3d::AfterRender()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);		
	glDisableVertexAttribArray(2);
}



//----------------------------------------------------------------------
//----------------------------------------------------------------------
Model3d::Model3d()
{
	m_vertexBuffer = 0;
	m_normalBuffer = 0;
	m_built = false;
}
//----------------------------------------------------------------------
Model3d::~Model3d()
{
	if(m_built)
	{
		glDeleteBuffers(1, &m_vertexBuffer);	
		glDeleteBuffers(1, &m_normalBuffer);
		glDeleteBuffers(1, &m_uvCoords);
	}
}
//----------------------------------------------------------------------
void Model3d::Build(const char *modelPath)
 {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	bool loadStatus = loadOBJ(modelPath, vertices, uvs, normals);
	if(loadStatus)
	{
		glGenBuffers(1, &m_vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &m_normalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);		

		glGenBuffers(1, &m_uvCoords);
		glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);	
	}

	m_vertexBufferSize = vertices.size();
	m_built = loadStatus;
 }
//----------------------------------------------------------------------
void Model3d::SetTexture(GLuint texture)
{
	m_texture = texture;
}
//----------------------------------------------------------------------
// const SceneLightData &sceneLightData, const glm::vec4 &clipPlane
void Model3d::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
						const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale)
{
	float rotationY = 0.0f;
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotateBase = glm::rotate(model, glm::radians(rot.x), glm::vec3(1, 0, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.y), glm::vec3(0, 1, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.z), glm::vec3(0, 0, 1));
	glm::mat4 translateBase = glm::translate(model, pos);

	glm::mat4 scaleBase = glm::scale(model, scale);
	model =  translateBase * rotateBase * scaleBase;
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProjection = projection * modelView;
	glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelView));

	// Forward to shader
	glUniformMatrix4fv(m_model3DShaderHandles.m_modelViewProjectionID, 1, GL_FALSE, &modelViewProjection[0][0]);	
	glUniformMatrix4fv(m_model3DShaderHandles.m_modelMatrixID, 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(m_model3DShaderHandles.m_viewMatrixID, 1, GL_FALSE, &view[0][0]);	
	
	glUniformMatrix4fv(m_model3DShaderHandles.m_modelViewMatrixID, 1, GL_FALSE, &modelView[0][0]);
	glUniformMatrix4fv(m_model3DShaderHandles.m_normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);	

	// Set clip plane (used only when rendering water reflection surface)
	glUniform4f(m_model3DShaderHandles.m_userClipPlane0ID, renderState.m_clipPlane.x, renderState.m_clipPlane.y, 
				renderState.m_clipPlane.z, renderState.m_clipPlane.w);	

	// Light
	glm::vec3 lightPos = renderState.m_sceneLightData.m_lightPos;
	if(renderState.m_invertLightPos)
	{
		lightPos.z = -lightPos.z;
	}

	glUniform3f(m_model3DShaderHandles.m_lightPos_worldspaceID, lightPos.x, lightPos.y, lightPos.z);

	glUniform3f(m_model3DShaderHandles.m_lightColorID, renderState.m_sceneLightData.m_lightColor.x, 
				renderState.m_sceneLightData.m_lightColor.y, renderState.m_sceneLightData.m_lightColor.z);
	
	// Active texture and bind the albedo texture
	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, m_texture);

	// Pass texture info on to the shader
	glUniform1i(m_model3DShaderHandles.m_textureID, 0);	

	// Enable vertex & normal attribute arrays		
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);		

	glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
	glVertexAttribPointer(
		2,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);		

	glDrawArrays(GL_TRIANGLES, 0, m_vertexBufferSize);
}

