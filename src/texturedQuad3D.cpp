#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "texturedQuad3D.h"
#include "lightData.h"
#include "renderStruct.h"
#include "shader.h"

// Statics
TexturedQuad3D::shaderDataSimpleLight TexturedQuad3D::m_simpleLightShader;
TexturedQuad3D::shaderDataTransparent TexturedQuad3D::m_transparentShader;


void TexturedQuad3D::InitTexturedQuad3D()
{
	m_simpleLightShader.m_programID = LoadShaders( "texturedQuadSimpleLight.vertexshader", 
													"texturedQuadSimpleLight.fragmentshader" );	

	// Get handles for data in the shader
	m_simpleLightShader.m_modelViewProjectionID = glGetUniformLocation(m_simpleLightShader.m_programID, "MVP");
	m_simpleLightShader.m_viewMatrixID = glGetUniformLocation(m_simpleLightShader.m_programID, "V");
	m_simpleLightShader.m_modelMatrixID = glGetUniformLocation(m_simpleLightShader.m_programID, "M");
	m_simpleLightShader.m_modelViewMatrixID = glGetUniformLocation(m_simpleLightShader.m_programID, "MV");
	m_simpleLightShader.m_normalMatrixID = glGetUniformLocation(m_simpleLightShader.m_programID, "NM");
	m_simpleLightShader.m_lightPos_worldspaceID = glGetUniformLocation(m_simpleLightShader.m_programID, "lightPosition_worldspace");
	m_simpleLightShader.m_lightColorID = glGetUniformLocation(m_simpleLightShader.m_programID, "lightColor");
	m_simpleLightShader.m_textureID = glGetUniformLocation(m_simpleLightShader.m_programID, "textureSampler");	
	m_simpleLightShader.m_userClipPlane0ID = glGetUniformLocation(m_simpleLightShader.m_programID, "userClipPlane0");

	//-------------------------------------------------------------------

	m_transparentShader.m_programID = LoadShaders( "texturedQuadTransparent.vertexshader", 
													"texturedQuadTransparent.fragmentshader" );	

	m_transparentShader.m_modelViewProjectionID = glGetUniformLocation(m_transparentShader.m_programID, "MVP");
	m_transparentShader.m_modelMatrixID = glGetUniformLocation(m_transparentShader.m_programID, "M");
	m_transparentShader.m_textureID = glGetUniformLocation(m_transparentShader.m_programID, "textureSampler");	
	m_transparentShader.m_userClipPlane0ID = glGetUniformLocation(m_transparentShader.m_programID, "userClipPlane0");
	m_transparentShader.m_alphaChannelID = glGetUniformLocation(m_transparentShader.m_programID, "alphaChannel");		
}
//----------------------------------------------------------------------
TexturedQuad3D::TexturedQuad3D(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 v4, float textureRepeat, GLuint texture)
{
	m_usingTransparentShader = false;
	m_alphaSetting = -1.0f; // Not using transparency	
	m_texture = texture;
	m_textureRepeat = textureRepeat;

	glm::vec3 normal = glm::cross(v2 - v1, v3 - v2);

	m_vertices.push_back(v1);
	m_vertices.push_back(v2);
	m_vertices.push_back(v3);

	m_vertices.push_back(v3);
	m_vertices.push_back(v4);
	m_vertices.push_back(v1);

	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_normals.push_back(normal);
	}
	
	CreateRectangleUVData();
	BuildVBOS();
}
//----------------------------------------------------------------------
TexturedQuad3D::TexturedQuad3D(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 v4, float textureRepeat, GLuint texture, 
					std::vector<glm::vec2> &verticesUV, float alphaSetting)
{
	m_usingTransparentShader = true;
	m_alphaSetting = alphaSetting;
	m_texture = texture;
	m_textureRepeat = textureRepeat;

	glm::vec3 normal = glm::cross(v2 - v1, v3 - v2);

	m_vertices.push_back(v1);
	m_vertices.push_back(v2);
	m_vertices.push_back(v3);

	m_vertices.push_back(v3);
	m_vertices.push_back(v4);
	m_vertices.push_back(v1);

	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_normals.push_back(normal);
	}

	m_verticesUV = verticesUV;
	BuildVBOS();
}
//----------------------------------------------------------------------
void TexturedQuad3D::CreateRectangleUVData()
{
	m_verticesUV.clear();
	glm::vec2 UV1(m_textureRepeat, 0.0f);
	glm::vec2 UV2(m_textureRepeat, m_textureRepeat);
	glm::vec2 UV3(0.0f, m_textureRepeat);
	m_verticesUV.push_back(UV1);
	m_verticesUV.push_back(UV2);
	m_verticesUV.push_back(UV3);

	glm::vec2 UV4(0.0f, m_textureRepeat);
	glm::vec2 UV5(0.0f, 0.0f);
	glm::vec2 UV6(m_textureRepeat, 0.0f);
	m_verticesUV.push_back(UV4);
	m_verticesUV.push_back(UV5);
	m_verticesUV.push_back(UV6);	
}
//----------------------------------------------------------------------
void TexturedQuad3D::BuildVBOS()
{
	glGenBuffers(1, &m_verticesVBO);
	glGenBuffers(1, &m_normalVBO);
	glGenBuffers(1, &m_verticesUVVBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec3), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_normalVBO);
	glBufferData(GL_ARRAY_BUFFER, m_normals.size() * sizeof(glm::vec3), &m_normals[0], GL_STATIC_DRAW);		

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec2), &m_verticesUV[0], GL_STATIC_DRAW);		
}
//----------------------------------------------------------------------
void TexturedQuad3D::PrepareRender(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view, 
									const glm::vec3 &pos)
{
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotateBase = glm::mat4(1.0f);
	glm::mat4 translateBase = glm::translate(model, pos);
	
	glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::mat4 scaleBase = glm::scale(model, scale);

	model =  translateBase * rotateBase * scaleBase;
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProjection = projection * modelView;

	if(m_usingTransparentShader)
	{
		glUseProgram(m_transparentShader.m_programID);	

		// Forward to shader
		glUniformMatrix4fv(m_transparentShader.m_modelViewProjectionID, 1, GL_FALSE, &modelViewProjection[0][0]);	
		glUniformMatrix4fv(m_transparentShader.m_modelMatrixID, 1, GL_FALSE, &model[0][0]);	

		// Set clip plane (used only when rendering water reflection surface)
		glUniform4f(m_transparentShader.m_userClipPlane0ID, renderState.m_clipPlane.x, renderState.m_clipPlane.y, 
					renderState.m_clipPlane.z, renderState.m_clipPlane.w);

		// Alpha texture setting
		glUniform1f(m_transparentShader.m_alphaChannelID, m_alphaSetting);	

		// Set "textureSampler" sampler to use Texture Unit 0
		glUniform1i(m_transparentShader.m_textureID, 0);			
	}
	else
	{
		glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelView));

		glUseProgram(m_simpleLightShader.m_programID);	

		// Forward to shader
		glUniformMatrix4fv(m_simpleLightShader.m_modelViewProjectionID, 1, GL_FALSE, &modelViewProjection[0][0]);	
		glUniformMatrix4fv(m_simpleLightShader.m_modelMatrixID, 1, GL_FALSE, &model[0][0]);
		glUniformMatrix4fv(m_simpleLightShader.m_viewMatrixID, 1, GL_FALSE, &view[0][0]);	
		
		glUniformMatrix4fv(m_simpleLightShader.m_modelViewMatrixID, 1, GL_FALSE, &modelView[0][0]);
		glUniformMatrix4fv(m_simpleLightShader.m_normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);	

		// Set clip plane (used only when rendering water reflection surface)
		glUniform4f(m_simpleLightShader.m_userClipPlane0ID, renderState.m_clipPlane.x, renderState.m_clipPlane.y, 
					renderState.m_clipPlane.z, renderState.m_clipPlane.w);

		// Light
		glUniform3f(m_simpleLightShader.m_lightPos_worldspaceID, renderState.m_sceneLightData.m_lightPos.x, 
						renderState.m_sceneLightData.m_lightPos.y, renderState.m_sceneLightData.m_lightPos.z);

		glUniform3f(m_simpleLightShader.m_lightColorID, renderState.m_sceneLightData.m_lightColor.x, 
						renderState.m_sceneLightData.m_lightColor.y, renderState.m_sceneLightData.m_lightColor.z);	

	}
}
//----------------------------------------------------------------------
void TexturedQuad3D::Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, 
							const glm::mat4 &view, const glm::vec3 &pos)
{
	if(renderState.m_blendOn)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}	

	if(renderState.m_disableDepthTest)
	{
		glDisable(GL_DEPTH_TEST);
	}

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_normalVBO);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, Texture());

	// Calculate and forward data to shaders
	PrepareRender(renderState, projection, view, pos);
	
	glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

	if(renderState.m_blendOn)
	{
		glDisable(GL_BLEND);
	}

	if(renderState.m_disableDepthTest)
	{
		glEnable(GL_DEPTH_TEST);
	}
}
//----------------------------------------------------------------------
GLuint TexturedQuad3D::Texture() const
{
	return m_texture;
}
//----------------------------------------------------------------------
void TexturedQuad3D::SetTexture(GLuint texture)
{
	m_texture = texture;
}
//----------------------------------------------------------------------
TexturedQuad3D::~TexturedQuad3D()
{
	glDeleteBuffers(1, &m_verticesVBO);
	glDeleteBuffers(1, &m_normalVBO);
	glDeleteBuffers(1, &m_verticesUVVBO);	
}