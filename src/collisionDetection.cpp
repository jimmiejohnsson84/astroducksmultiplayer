#include <iostream>
#include <memory>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

#include "sphere.h"
#include "duck.h"
#include "collisionDetection.h"


bool SphereSphereTest(const glm::vec3 &s1Pos, float s1Radius, const glm::vec3 &s2Pos, float s2Radius)
{
	glm::vec3 dist = s1Pos - s2Pos;

	float sumRadiusSquare = s1Radius + s2Radius;
	sumRadiusSquare = sumRadiusSquare * sumRadiusSquare;

	return dist.x * dist.x + dist.y * dist.y + dist.z * dist.z <= sumRadiusSquare;	
}

bool SphereBorderTest(const glm::vec3 &sphere, float sphereRadius, float xMin, float yMin, float xMax, float yMax)
{
	float distXMin = sphere.x - xMin;
	float distXMax = sphere.x - xMax;

	float distYMin = sphere.y - yMin;
	float distYMax = sphere.y - yMax;

	return (fabs(distXMin) <= sphereRadius || fabs(distXMax) <= sphereRadius 
			|| fabs(distYMin) <= sphereRadius || fabs(distYMax) <= sphereRadius);
}