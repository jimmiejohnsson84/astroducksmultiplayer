#ifndef MODEL3D_COOK_TORRANCE_H
#define MODEL3D_COOK_TORRANCE_H


// Fwds
struct RenderStateModel3D;

class Model3dCookTorrance
{
	public:
	Model3dCookTorrance();
	~Model3dCookTorrance();

	void	Build(const char *modelPath);
	void	SetTexture(GLuint textureAlbedo, GLuint textureNormal, GLuint textureMetallic, GLuint textureRoughness, GLuint textureAO);

	void	Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
					const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale);

	static void InitModel3dCookTorrance();
	static void PrepareRender();
	static void AfterRender();

	private:

	// BOs containing vertex and normal data for the model
	GLuint	m_elementBuffer;
	GLuint	m_normalBuffer;
	GLuint	m_vertexBuffer;
	GLuint	m_uvCoords;
	GLuint	m_tangents;

	// Material description (textures)
	GLuint m_textureAlbedo;
	GLuint m_textureNormal;
	GLuint m_textureMetallic;
	GLuint m_textureRoughness;
	GLuint m_textureAO;

	unsigned int	m_vertexBufferSize;
	unsigned int	m_indicesSize;

	// Shader handles
	struct shaderProgramHandles
	{
		GLuint m_programID;
		GLuint m_lightPos_worldspaceID;
		GLuint m_viewMatrixID;
		GLuint m_modelViewProjectionID;
		GLuint m_modelMatrixID;
		GLuint m_modelViewMatrixID;
		GLuint m_modelView3By3MatrixID;
		GLuint m_normalMatrixID;
		GLuint m_userClipPlane0ID;

		GLuint m_lightColorID;
		GLuint m_lightIntensityID;

		// Textures
		GLuint m_textureAlbedoID;
		GLuint m_textureNormalID;
		GLuint m_textureMetallicID;
		GLuint m_textureRoughnessID;
		GLuint m_textureAOID;
	};

	static shaderProgramHandles m_model3DCookTorranceShaderHandles;
	
	// If true, BOs have been built
	bool			m_built;
};

#endif