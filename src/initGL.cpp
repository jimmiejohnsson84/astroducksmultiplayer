#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <fstream>

#include "initGL.h"

int InitAndSetHints(std::ofstream *outputFileDebug)
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		if(!outputFileDebug)
		{
			cout << "Failed to initialize GLFW" << endl;
		}
		else
		{
			*outputFileDebug << "Failed to initialize GLFW" << endl;
		}
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	return 1;
}

int CreateglfwWindowAndMakeCurrent(GLFWwindow** window, int windowWidth, int windowHeight, const char *windowTitle, std::ofstream *outputFileDebug)
{
	GLFWmonitor* primary = glfwGetPrimaryMonitor();

	// Open a window and create its OpenGL context
	// Pass primary on to make fullscreen, NULL to make window mode
		
	// Open a window and create its OpenGL context
	*window = glfwCreateWindow(windowWidth, windowHeight, windowTitle, NULL, NULL);	
	if( *window == NULL )
	{
		if(!outputFileDebug)
		{
			cout << "Failed to open GLFW window" << endl;
		}
		else
		{
			*outputFileDebug << "Failed to open GLFW window" << endl;
		}
		glfwTerminate();
		return -1;
	}

	if(outputFileDebug)
	{
		*outputFileDebug << "calling glfwMakeContextCurrent" << endl;
	}
	
	glfwMakeContextCurrent(*window);
    
    return 1;
}

int InitGlewAndInputMode(GLFWwindow* window, std::ofstream *outputFileDebug)
{
	// Initialize GLEW
	glewExperimental = true;
	if (glewInit() != GLEW_OK) 
	{
		if(!outputFileDebug)
		{
			cout << "Failed to initialize GLEW" << endl;
		}
		else
		{
			*outputFileDebug << "Failed to open GLFW window" << endl;			
		}
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    
    return 1;
}

int InitGL(GLFWwindow** window, int windowWidth, int windowHeight, const char *windowTitle, std::ofstream *outputFileDebug)
{
	if(outputFileDebug)
	{
		*outputFileDebug << "Setting hints" << endl;
	}
	if(InitAndSetHints(outputFileDebug) == -1)
	{
		return -1;
	}

	if(outputFileDebug)
	{
		*outputFileDebug << "Calling CreateglfwWindowAndMakeCurrent" << endl;
	}

	if(CreateglfwWindowAndMakeCurrent(window, windowWidth, windowHeight, windowTitle, outputFileDebug) == -1)
	{
		return -1;
	}

	if(outputFileDebug)
	{
		*outputFileDebug << "Calling InitGlewAndInputMode" << endl;
	}

	if(InitGlewAndInputMode(*window, outputFileDebug) == -1)
	{
		return -1;
	}

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	// Enable culling
	glEnable(GL_CULL_FACE);
	

	return 1;	
}