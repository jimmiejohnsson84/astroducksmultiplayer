#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

#include "texturedQuad3D.h"
#include "poolWalls.h"
#include "lightData.h"
#include "texture.h"
#include "gameDefs.h"


PoolWalls::PoolWalls()
{
	double zPosNear = 0.0f;
	double zPosFar = -15.0f;
	TexturedQuad3D* wallBottom = new TexturedQuad3D(glm::vec3(WORLD_XMIN, WORLD_YMIN, zPosFar), glm::vec3(WORLD_XMAX, WORLD_YMIN, zPosFar),
												glm::vec3(WORLD_XMAX, WORLD_YMAX, zPosFar), glm::vec3(WORLD_XMIN, WORLD_YMAX, zPosFar),
												4.0f, GetTextureResource(POOL_WALLS_BOTTOM));


	TexturedQuad3D* wallLeft = new TexturedQuad3D(glm::vec3(WORLD_XMIN, WORLD_YMIN, zPosNear), glm::vec3(WORLD_XMIN, WORLD_YMIN, zPosFar),
												glm::vec3(WORLD_XMIN, WORLD_YMAX, zPosFar), glm::vec3(WORLD_XMIN, WORLD_YMAX, zPosNear),
												4.0f, GetTextureResource(POOL_WALLS_TEXTURE));


	TexturedQuad3D* wallRight = new TexturedQuad3D(glm::vec3(WORLD_XMAX, WORLD_YMIN, zPosFar), glm::vec3(WORLD_XMAX, WORLD_YMIN, zPosNear),
												glm::vec3(WORLD_XMAX, WORLD_YMAX, zPosNear), glm::vec3(WORLD_XMAX, WORLD_YMAX, zPosFar),
												4.0f, GetTextureResource(POOL_WALLS_TEXTURE));


	TexturedQuad3D* wallUpperSide = new TexturedQuad3D(glm::vec3(WORLD_XMAX, WORLD_YMAX, zPosNear), glm::vec3(WORLD_XMIN, WORLD_YMAX, zPosNear), 
												glm::vec3(WORLD_XMIN, WORLD_YMAX, zPosFar), glm::vec3(WORLD_XMAX, WORLD_YMAX, zPosFar),
												4.0f, GetTextureResource(POOL_WALLS_TEXTURE));

	TexturedQuad3D* wallLowerSide = new TexturedQuad3D(glm::vec3(WORLD_XMIN, WORLD_YMIN, zPosNear), glm::vec3(WORLD_XMAX, WORLD_YMIN, zPosNear), 
												glm::vec3(WORLD_XMAX, WORLD_YMIN, zPosFar), glm::vec3(WORLD_XMIN, WORLD_YMIN, zPosFar),
												4.0f, GetTextureResource(POOL_WALLS_TEXTURE));

	m_poolWalls.push_back(wallBottom);
	m_poolWalls.push_back(wallLeft);
	m_poolWalls.push_back(wallRight);
	m_poolWalls.push_back(wallUpperSide);
	m_poolWalls.push_back(wallLowerSide);
}
//----------------------------------------------------------------------
PoolWalls::~PoolWalls()
{
	for(auto poolWall : m_poolWalls)
	{
		delete poolWall;
	}
}
//----------------------------------------------------------------------
void PoolWalls::Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view)
{
	const glm::vec3 pos(0.0f, 0.0f, 0.0f);
	for(auto poolWall : m_poolWalls)
	{
		poolWall->Render(renderState, projection, view, pos);
	}
}
